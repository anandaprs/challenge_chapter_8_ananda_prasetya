const { Car } = require("../app/models");

// Testing Read
describe("GET /v1/cars", async function() {
    test("Gets a list of 1 car", async function() {
      const response = await request(app).get(`//v1/cars`);
      const { cars } = response.body;
      expect(response.statusCode).toBe(200);
      expect(cars).toHaveLength(1);
      expect(cars[0]).toEqual(Car);
    });
  });

  describe("GET /v1/cars/:id", async function() {
    test("Gets a single car", async function() {
      const response = await request(app).get(`/v1/cars/${Car.id}`);
      expect(response.statusCode).toBe(200);
      expect(response.body.Car).toEqual(Car);
    });
  
    test("Responds with 404 if can't find car", async function() {
      const response = await request(app).get(`/v1/cars/0`);
      expect(response.statusCode).toBe(404);
    });
  });

// Testing Create
describe("POST /v1/cars", async function() {
    test("Creates a new car", async function() {
      const response = await request(app)
        .post(`/v1/cars`)
        .send({
          name: "Nan"
        });
      expect(response.statusCode).toBe(201);
      expect(response.body.Car).toHaveProperty("id");
      expect(response.body.Car).toHaveProperty("name");
      expect(response.body.Car.name).toEqual("Nan");
    });
  });

// Testing Update
describe("PUT /v1/cars/:id", async function() {
    test("Updates a single car", async function() {
      const response = await request(app)
        .put(`/v1/cars/${Car.id}`)
        .send({
          name: "Me"
        });
      expect(response.statusCode).toBe(200);
      expect(response.body.Car).toEqual({
        id: Car.id, name: "Me"
      });
    });
  
    test("Responds with 404 if can't find cat", async function() {
      const response = await request(app).put(`/v1/cars/0`);
      expect(response.statusCode).toBe(404);
    });
  });

// Testing Delete
describe("DELETE /v1/cars/:id", async function() {
    test("Deletes a single a car", async function() {
      const response = await request(app)
        .delete(`/v1/cars/${Car.id}`);
      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({ message: "Car deleted" });
    });
  });
