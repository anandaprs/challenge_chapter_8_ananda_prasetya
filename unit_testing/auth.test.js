const { AuthenticationController } = require ("../app/controllers/AuthenticationController")
const { EmailNotRegisteredError, InsufficientAccessError, RecordNotFoundError, WrongPasswordError } = require("../app/errors");
const { JWT_SIGNATURE_KEY } = require("../config/application");
let auth = {};
    
beforeEach(async function() {
  const hashedPassword = await bcrypt.hash("secret", 1);
  await db.query(
    `INSERT INTO users (username, password)
        VALUES ('test', $1)`,
    [hashedPassword]);
  const response = await request(app)
    .post("/login")
    .send({
      username: "test",
      password: "secret"
    });
  
  // we'll need the token for future requests
  auth.token = response.body.token;

  // we'll need the user_id for future requests
  auth.curr_user_id = "test"
});

describe("GET /secret success", async function() {
    test("returns 'You made it'", async function() {
      const response = await request(app)
        .get(`/secret`)
        .send({ _token: auth.token });
      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({ message: "You made it!"});
    });
  });

  describe("GET /secret failure", async function() {
    test("returns 401 when logged out", async function() {
      const response = await request(app)
        .get(`/secret`); // no token being sent!
      expect(response.statusCode).toBe(401);
      expect(response.body.error).toEqual({
        message: "Unauthorized", status: 401
      });
    });
  
    test("returns 401 with invalid token", async function() {
      const response = await request(app)
        .get(`/secret`)
        .send({ _token: "garbage"}); // invalid token!
      expect(response.statusCode).toBe(401);
      expect(response.body.error).toEqual({
        message: "Unauthorized", status: 401
      });
    });
  });

  describe("GET /users/:username, valid token", async function() {
    test(
      "returns 'You made it' when logged in as the correct user", 
      async function() {
        const response = await request(app)
          .get(`/users/test`)
          .send({ _token: auth.token });
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual({ message: "You made it!"});
      });
    
    test("returns 401 with mismatch in URL", async function() {
      const response = await request(app)
        .get(`/users/test2`)
        .send({ _token: auth.token}); // mismatch!
      expect(response.statusCode).toBe(401);
      expect(response.body.error).toEqual({
        message: "Unauthorized", status: 401
      });
    });
  });

describe("GET /users/:uname, invalid token", async function() {
  test("returns 401 when logged out", async function() {
    const response = await request(app)
      .get(`/users/test`); // no token!
    expect(response.statusCode).toBe(401);
    expect(response.body.error).toEqual({
      message: "Unauthorized", status: 401
    });
  });

  test("returns 401 with invalid token", async function() {
    const response = await request(app)
      .get(`/users/test`)
      .send({ _token: "garbage"}); // invalid token!
    expect(response.statusCode).toBe(401);
    expect(response.body.error).toEqual({
      message: "Unauthorized", status: 401
    });
  });
});